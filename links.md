# Angular Boot Camp

[Angular Boot Camp Curriculum](https://github.com/angularbootcamp/abc)

[Angular Boot Camp Zip File](http://angularbootcamp.com/abc.zip)

[Video Manager](http://videomanager.angularbootcamp.com)

[Workshop Repository](https://bitbucket.org/od-training/abc-pt20200720)

[Video Data](https://api.angularbootcamp.com/videos)

[Survey](https://docs.google.com/forms/d/e/1FAIpQLSdv6vh0DjAEt8uakJ7h3M-5XuFPW5H4CPxh_nD7Tn_X_En__Q/viewform)

## Resources

[DOM Events](https://developer.mozilla.org/pt-PT/docs/Web/Eventos)

[Angular CLI talk](https://www.youtube.com/watch?v=LKY0oc5snI4)

[Angular 7 Lazy Loading](https://www.techiediaries.com/angular-routing-lazy-loading-loadchildren/)

[Angular Advice Blog](https://medium.com/@tomwhite007/angular-standards-for-2020-d31f401fb90d)

[Component Interact](https://angular.io/guide/component-interaction)

[Olivier Combe on i18n](https://github.com/ngx-translate/core/issues/783)

[Transloco comparison matrix](https://netbasal.gitbook.io/transloco/comparison-to-other-libraries)

## Observables

[Cory Rylan Video on Observables](https://www.youtube.com/watch?v=-yY2ECd2tSM)

[Ben Lesh Video on creating Observables](https://www.youtube.com/watch?v=m40cF91F8_A)

[Book: Build Reactive Websites with RxJS](https://pragprog.com/book/rkrxjs/build-reactive-websites-with-rxjs)

[I switched a map and you'll never guess what happened next (SwitchMap video)](https://www.youtube.com/watch?v=rUZ9CjcaCEw)

[RXMarbles](http://rxmarbles.com)

[LearnRXJS](https://www.learnrxjs.io/)

[Seven Operators to Get Started with RxJS (Article)](https://www.infoq.com/articles/rxjs-get-started-operators)

[Reactive Visualizations](https://reactive.how/)

[Operator Decision Tree](https://rxjs-dev.firebaseapp.com/operator-decision-tree)

## Utilities

[json2ts (generate TypeScript interfaces from JSON)](http://json2ts.com/)

[Transloco i18n tool](https://github.com/ngneat/transloco)

## VS Code extensions

[Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template)

[angular2-switcher](https://marketplace.visualstudio.com/items?itemName=infinity1207.angular2-switcher)

[Bracket Pair Colorizer
2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)

## Learning Resources

[Loiane Groner (Brazilian Angular YouTube)](https://www.youtube.com/loianegroner)

### Conferences

[Angular Conferences](https://angularconferences.com/)

### Podcasts/Videocasts

[The Angular Show](https://www.spreaker.com/show/angular-show)

[Angular Air (Videocast)](https://angularair.com/)

[Adventures in Angular (Podcast)](https://devchat.tv/adv-in-angular)

[Real Talk JavaScript](https://realtalkjavascript.simplecast.fm/)

[Code Talk Teach (Angular Boot Camp curriculum discussions)](https://www.youtube.com/channel/UCC3ihTPwdU0PU9P7QGJa7bw)

### Newsletters

[ng-newsletter (weekly Angular email)](https://www.ng-newsletter.com/)

[Angular Top 5 (weekly Angular email)](http://angulartop5.com/)

### Blogs

[Angular In Depth](https://blog.angularindepth.com/)

[Netanel Basal](https://netbasal.com/)

[Cory Rylan](https://coryrylan.com/)
