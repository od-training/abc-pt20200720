import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap, filter, share, shareReplay } from 'rxjs/operators';
import { VideoDataService } from 'src/app/video-data.service';
import { Video } from '../interfaces';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css'],
})
export class VideoPlayerComponent {
  video: Observable<Video>;
  today = new Date();
  url: Observable<SafeUrl>;

  constructor(
    route: ActivatedRoute,
    svc: VideoDataService,
    sanitizer: DomSanitizer
  ) {
    this.video = route.queryParamMap.pipe(
      map((params) => params.get('selectedId')),
      filter((id) => !!id),
      switchMap((id) => svc.getVideo(id)),
      shareReplay(1)
    );
    this.url = this.video.pipe(
      map((video) => {
        return sanitizer.bypassSecurityTrustResourceUrl(
          'https://www.youtube.com/embed/' + video.id
        );
      })
    );
  }
}
