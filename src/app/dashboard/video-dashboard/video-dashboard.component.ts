import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { VideoDataService } from 'src/app/video-data.service';
import { Video } from '../interfaces';
import { Router, ActivatedRoute } from '@angular/router';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css'],
})
export class VideoDashboardComponent {
  videos: Observable<Video[]>;

  constructor(svc: VideoDataService, router: Router, route: ActivatedRoute) {
    this.videos = svc.videos$.pipe(
      tap((videos) => {
        if (!route.snapshot.queryParamMap.get('selectedId')) {
          router.navigate([], {
            queryParams: {
              selectedId: videos[0].id,
            },
          });
        }
      })
    );
  }
}
