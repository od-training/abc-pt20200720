import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Video } from '../interfaces';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css'],
})
export class VideoListComponent {
  @Input() videos: Video[] = [];
  selectedVideoId: Observable<string>;

  constructor(private router: Router, route: ActivatedRoute) {
    this.selectedVideoId = route.queryParamMap.pipe(
      map((params) => params.get('selectedId'))
    );
  }

  selectVideo(video: Video) {
    this.router.navigate([], {
      queryParams: {
        selectedId: video.id,
      },
    });
  }
}
