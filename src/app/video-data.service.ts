import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Video } from './dashboard/interfaces';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const apiUrl = 'https://api.angularbootcamp.com';

@Injectable({
  providedIn: 'root',
})
export class VideoDataService {
  videos$: Observable<Video[]>;

  constructor(private http: HttpClient) {
    this.videos$ = http.get<Video[]>(apiUrl + '/videos').pipe(
      map((videos) => {
        return (
          videos
            // include only the videos that start with 'Angular'
            .filter((video) => {
              return video.title.startsWith('Angular');
            })
            // use the spread operator to create a shallow copy,
            // then change the title to all caps
            .map((video) => {
              return {
                ...video,
                title: video.title.toUpperCase(),
              };
            })
        );
      })
    );
  }

  getVideo(id: string): Observable<Video> {
    return this.http.get<Video>(`${apiUrl}/videos/${id}`);
  }
}
